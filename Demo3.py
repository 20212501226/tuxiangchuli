import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
#对图像进行旋转
#读取图像，并转化为数组
i=np.array(Image.open("kanghui.jpg"))
#旋转180度
j=i[::-1]
#数组转图片
j1=Image.fromarray(j.astype('uint8'))
#保存图片
j1.save('kuanghui.jpg')
j1.show()
#向左旋转90度
#交换数组的第一维和第二维
y=i.swapaxes(0,1)
#进行上下方向的对称转换
z=y[::-1]
#输出左转90度后的图像
plt.imshow(z)
plt.show()
#右转90度
#交换数组的第一维和第二维
m=i.swapaxes(0,1)
#进行左右方向的对称转换
u=m[:,::-1]
#输出左转90度后的图像
plt.imshow(u)
plt.show()