import numpy as np
import matplotlib.pyplot as plt
#对图像进行灰色处理
#读取图片
i=plt.imread("kanghui.jpg")
plt.imshow(i)
i1=np.array([0.299,0.587,0.114])
#对数组进行点积运算，数组的最低维RGB与上面的一维数组进行对位相乘再相加
i2=np.dot(i,i1)
plt.imshow(i2,cmap="gray")
plt.show()#展示图片