import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
#对图像进行对称处理
#读取图像，并返回图像数组
i=plt.imread(r"kanghui.jpg")
#水平对称行不变，列进行颠倒,交换第一维和第二维
y=i.swapaxes(0,1)
#定义子区间的个数将两张图片放在同一水平线上，在画布上左右方向上进行显示
fig,ax=plt.subplots(1,2)
#设置图片的高度及宽度
fig.set_size_inches(20,10)
#在左边放置原图像
ax[0].imshow(i)
#为原图像设置标题为原来的图像
ax[0].set_title("原来的图像")
#对列进行颠倒在右边放置变化后的图像
ax[1].imshow(i[:,::-1])
#为原图像设置标题为原来的图像
ax[1].set_title("水平变化后的图像")
#垂直变化，列不变，对行进行颠倒
#定义子区间的个数将两张图片放在同一垂直线上，在画布上下方向上进行显示
fig,ax=plt.subplots(2,1)
#设置图片的高度及宽度
fig.set_size_inches(15,15)
#在上边放置原图像
ax[0].imshow(i)
#为原图像设置标题为原来的图像
ax[0].set_title("原来的图像")
#对行进行颠倒再下面放置变化后的图像
ax[1].imshow(i[::-1])
#为原图像设置标题为原来的图像
ax[1].set_title("垂直变化后的图像")
plt.show()