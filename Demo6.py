import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
#读取图像，并返回图像数组
i=plt.imread("kanghui.jpg")
m=i.copy()
#提取通道RGB三个通道，R为红色，G为绿色，B为蓝色R通道从红色的角度显示图片
#提取红色通道
r=i.copy()
r[:,:,1:3]=0
#提取绿色通道
g=i.copy()
g[:,:,[0,2]]=0
#提取蓝色通道
b=i.copy()
b[:,:,:2]=0
#定义子区间的个数将四张图片按照指定位置进行存放
fig,ax=plt.subplots(2,2)
#设置图片的高度及宽度
fig.set_size_inches(25,15)
#在第一行第一列的位置显示原图像
ax[0,0].imshow(i)
#为原图像设置标题
ax[0,0].set_title("原来的图像")
#在第一行第二列的位置显示提取绿色通道后的图像
ax[0,1].imshow(g)
#为提取绿色通道后的图像设置标题
ax[0,1].set_title("提取绿色通道后的图像")
#在第二行第一列的位置显示提取绿色通道后的图像
ax[1,0].imshow(b)
#为提取蓝色通道后的图像设置标题
ax[1,0].set_title("提取蓝色通道后的图像")
#在第二行第二列的位置显示提取红色通道后的图像
ax[1,1].imshow(r)
#为提取红色通道后的图像设置标题
ax[1,1].set_title("提取绿色通道后的图像")
plt.show()


