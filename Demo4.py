import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
#对图片进行拼接
i=plt.imread(r"kanghui.jpg")#读取图像，返回图像的数组
#输出图像的形状
print(i.shape)
#将图像沿水平方向重复三次1表示水平方向
j=np.concatenate((i,i,i),axis=1)
#输出图像的形状
print(j.shape)
#将图像沿垂直方向重复三次0表示垂直方向
k=np.concatenate((i,i,i),axis=0)
#输出图像的形状
print(k.shape)
#对图像沿垂直方向重复两次
n=np.concatenate((i,i),axis=0)
#输出图像的形状
print(n.shape)
fig,ax=plt.subplots(2,2)
fig.set_size_inches(6,6)#画布大小
#显示经过四种后变化后各图片的位置
ax[0,0].imshow(i)
ax[0,1].imshow(j)
ax[1,0].imshow(k)
ax[1,1].imshow(n)
plt.tight_layout()#自动调节图片之间的间距
plt.show()